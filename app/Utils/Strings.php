<?php

namespace App\Utils;

use Illuminate\Support\Facades\Validator;

class Strings {

    /**
     * Create a random and unique string
     *
     * @param $rules
     * @param $amount
     * @return string (unique)
     */
    public static function randUnique($rules, $amount)
    {
        $page['name'] = str_random($amount);

        $v = Validator::make($page, [
            'name' => $rules
        ]);

        if ($v->fails()) {
            $page['name'] = str_rand_unique($rules, $amount);
        }

        return $page['name'];
    }
}