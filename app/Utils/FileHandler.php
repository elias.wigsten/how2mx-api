<?php

namespace App\Utils;

use Intervention\Image\ImageManagerStatic as Image;

class FileHandler {
    private $path = '/Users/how2mx/Documents/Plugg/workspace/how2mx/project/storage/';
    private $filename;
    private $extension = '.png';
    private $files = [];

    public function __construct($path = null)
    {
        $this->path = !empty($path) ? $this->path . implode('/', $path) . '/' : $this->path;
    }

    public function resizeAndSaveImageToDisk($file)
    {
        $this->filename = Generator::UUID();

        $this->files['original'] = Image::make($file->getPathname());
        $this->files['preview'] = Image::make($file->getPathname());
        $this->files['thumbnail'] = Image::make($file->getPathname());

        $this->files['preview']->fit(400, 400);
        $this->files['thumbnail']->fit(100, 100);

        $this->save();
    }

    private function save()
    {
        $this->createDirectories();

        foreach ($this->files AS $subDir => $file) {
            $file->save($this->path . $subDir . '/' . $this->filename . $this->extension, 100);
        }
    }

    private function createDirectories()
    {
        if (!file_exists($this->path)) {
            mkdir($this->path, 0775, true);
            mkdir($this->path . 'original/', 0775, true);
            mkdir($this->path . 'preview/', 0775, true);
            mkdir($this->path . 'thumbnail/', 0775, true);
        }
    }

    public function getFileSrc()
    {
        return $this->filename . $this->extension;
    }
}