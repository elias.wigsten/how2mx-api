<?php

namespace App\Utils;

class Generator {
    public static function UUID() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    public static function UUIDv2()
    {
        static $inc = 0;
        $UUID = '';

        $ts = pack('N', time());
        $m = substr(md5(gethostname()), 0, 3);
        $pid = pack('n', posix_getpid());
        $trail = substr(pack('N', $inc++), 1, 3);
        $bin = sprintf("%s%s%s%s", $ts, $m, $pid, $trail);

        for ($i = 0; $i < 12; $i++) {
            $UUID .= sprintf("%02X", ord($bin[$i]));
        }

        return $UUID;
    }

    public static function str_rand_unique($rules, $amount)
    {
        $page['name'] = str_random($amount);

        $v = Validator::make($page, [
            'name' => $rules
        ]);

        if ($v->fails()) {
            $page['name'] = str_rand_unique($rules, $amount);
        }

        return $page['name'];
    }
}