<?php

namespace App\Models;

use App\Utils\Generator;
use App\Utils\Strings;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends UuidModel implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'f_name',
        'l_name',
        'page_id',
        'gender',
        'email',
        'birth_date',
        'phone',
        'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'activation_code'
    ];

    public function getFNameAttribute($name)
    {
        return uc_name($name);
    }

    public function getLNameAttribute($name)
    {
        return uc_name($name);
    }

    public function getBirthDateAttribute($date)
    {
        return user_age($date);
    }

    public function getGenderAttribute($gender)
    {
        if ($gender == 0) {
            return 'Male';
        }
        return 'Female';
    }

    public static function createUser($data)
    {
        $user = new User();
        $user->id = Generator::UUID();
        $user->f_name = $data->f_name;
        $user->l_name = $data->l_name;
        $user->page_id = $data->page_id;
        $user->gender = $data->gender;
        $user->email = $data->email;
        $user->birth_date = $data->birth_date;
        $user->password = bcrypt($data->password);
        $user->activation_code = Strings::randUnique('unique:users,activation_code', 60);;
        $user->save();

        return $user;
    }

    public static function getUserForAuthorized($page_id, $self_id)
    {
        $user = User::where('page_id', $page_id)->first();

        $user->friendship = Friendship::where(['user1' => $self_id, 'user2' => $user->id])
                                ->orWhere(['user1' => $user->id, 'user2' => $self_id])
                                ->first();

        return $user;
    }

    public static function getUserForGuest($page_id)
    {
        return User::where('page_id', $page_id)
                    ->first();
    }

    public static function getUserBy($page_id, $self_id)
    {
        return User::where('page_id', '=', $page_id)
                    ->join('friendships', function($join) {
                        $join->__construct('left outer', 'friendships');
                        $join->on('users.id', '=', 'friendships.user1')
                            ->orOn('users.id', '=', 'friendships.user2');
                    })
                    ->first();
    }
}
