<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

class Gallery extends UuidModel
{
    protected $table = 'galleries';

    protected $fillable = [
        'id',
        'page_id',
        'title'
    ];

    public function items()
    {
        return $this->hasMany('App\Models\MediaItem');
    }

    public function preview()
    {
        return $this->hasOne('App\Models\MediaItem', 'id', 'preview');
    }

    public static function canRefresh()
    {
        if (empty($gallery)) {
            return false;
        }

        $count = Gallery::where('page_id', $gallery->page_id)
            ->where('created_at', '<=', $gallery->created_at)
            ->where('id', '!=', $gallery->id)
            ->count();

        return $count > 0;
    }

    public static function countYoungerThan($gallery)
    {
        return Gallery::where('page_id', $gallery->page_id)
            ->where('created_at', '>=', $gallery->created_at)
            ->where('id', '!=', $gallery->id)
            ->count();
    }

    public static function createNew()
    {
        $gallery = new Gallery();
        $gallery->page_id = Auth::user()->page_id;
        $gallery->title = 'Untitled';
        $gallery->save();

        return $gallery;
    }
}
