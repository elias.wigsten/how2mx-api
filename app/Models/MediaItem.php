<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MediaItem extends Model {
    protected $hidden = [
        'owner_id',
        'gallery_id'
    ];

    protected $table = 'media_items';

    protected $fillable = [
        'gallery_id',
        'media_item_type_id',
        'src',
        'description'
    ];

    public function getMediaTypeAttribute($mediaType)
    {
        return $mediaType === 1 ? 'img' : 'vid';
    }

    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery')->withTimestamps();
    }

    public static function findLast($page_id, $gallery_id)
    {
        return MediaItem::where('owner_id', $page_id)
            ->where('gallery_id', $gallery_id)
            ->orderBy('id', 'asc')
            ->first();
    }

    public static function itemsBetween($owner_id, $gallery_id, $from, $to)
    {
        $count = 0;

        if (!empty($from) && !empty($to)) {
            $count = MediaItem::where('owner_id', $owner_id)
                ->where('gallery_id', $gallery_id)
                ->where('id', '<', $from)
                ->where('id', '>', $to)
                ->count();
        }

        return $count > 0;
    }

    public static function itemsAfter($owner_id, $gallery_id, $item_id)
    {
        $count = 0;

        if (!empty($owner_id) && !empty($gallery_id) && !empty($item_id)) {
            $count = MediaItem::where('owner_id', $owner_id)
                ->where('gallery_id', $gallery_id)
                ->where('id', '<', $item_id)
                ->count();
        }

        return $count > 0;
    }

    public static function itemsBefore($item)
    {
        $count = 0;

        if (!empty($item)) {
            $count = MediaItem::where('owner_id', $item->owner_id)
                ->where('gallery_id', $item->gallery_id)
                ->where('id', '>', $item->id)
                ->count();
        }

        return $count > 0;
    }

    public static function newImg($gallery_id, $src)
    {
        $item = new MediaItem();
        $item->src = $src;
        $item->description = '';
        $item->gallery_id = $gallery_id;
        $item->owner_id = Auth::user()->page_id;
        $item->media_type = 1;
        $item->save();

        return $item;
    }
}