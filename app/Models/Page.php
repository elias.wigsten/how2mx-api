<?php

namespace App\Models;

use App\Utils\Generator;

class Page extends UuidModel
{
    public static function createPage()
    {
        $page = new Page();
        $page->save();

        return $page->id;
    }
}
