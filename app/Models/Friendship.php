<?php
namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Config\Definition\Exception\Exception;

class Friendship extends Model {

    protected $table = 'friendships';

    public static function addFriend($user1, $user2)
    {
        $values = array(
            'user1'     => $user1,
            'user2'     => $user2,
            'status'    => 0
        );

        DB::table('friendships')->insert($values);

        return $values;
    }

    public static function removeFriend($user1, $user2)
    {
        return DB::table('friendships')
                        ->where(['user1' => $user1, 'user2' => $user2])
                        ->orWhere(['user1' => $user2, 'user2' => $user1])
                        ->delete();
    }

    public static function acceptFriend($self, $user)
    {
        $updated = DB::table('friendships')
                        ->where(['user1' => $user, 'user2' => $self])
                        ->update(['status' => 1]);

        if ($updated) {
            return array(
                'user1'     => $user,
                'user2'     => $self,
                'status'    => 1
            );
        }

        return false;
    }

    public static function getUserFriends($user_id)
    {
        $stm1 = User::where('user1', $user_id)
                        ->where('status', 1)
                        ->join('friendships', 'users.id', '=', 'friendships.user2');

        return User::where('user2', $user_id)
                        ->where('status', 1)
                        ->join('friendships', 'users.id', '=', 'friendships.user1')
                        ->union($stm1)
                        ->get();
    }

    public static function getSourceUserFriends($user_id)
    {
        $stm1 = User::where('user1', $user_id)
            ->join('friendships', 'users.id', '=', 'friendships.user2');

        return User::where('user2', $user_id)
            ->join('friendships', 'users.id', '=', 'friendships.user1')
            ->union($stm1)
            ->orderBy('status', 'desc')
            ->get();
    }

    public static function existsBetween($self_id, $user_id)
    {
        return Friendship::where(['user1' => $self_id, 'user2' => $user_id])
                    ->orWhere(['user1' => $user_id, 'user2' => $self_id])
                    ->first();
    }
}