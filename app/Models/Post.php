<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    private static $userPostFields = [
        'posts.id AS post_id',
        'posts.page_id AS to_page',
        'posts.message',
        'posts.created_at',
        'posts.updated_at',
        'users.id AS user_id',
        'users.page_id',
        'users.profile_img',
        'users.f_name',
        'users.l_name',
        'users.page_id'
    ];

    private static $test = [
        'posts.id AS post_id',
        'posts.message',
        'posts.created_at',
        'posts.updated_at',
        'author.page_id AS author_id',
        'author.profile_img AS author_profile_img',
        'author.f_name AS author_f_name',
        'author.l_name AS author_l_name',
        'recipient.page_id AS recipient_id',
        'recipient.profile_img AS recipient_profile_img',
        'recipient.f_name AS recipient_f_name',
        'recipient.l_name AS recipient_l_name',
    ];

    public static function getUserPosts($page_id)
    {
        return Post::join('users AS author', 'author.page_id', '=', 'posts.author_id')
                    ->join('users AS recipient', 'recipient.page_id', '=', 'posts.page_id')
                    ->where('posts.page_id', $page_id)
                    ->orderBy('posts.created_at', 'desc')
                    ->select(Post::$test)
                    ->get();
    }

    public static function remove($page_id, $post_id)
    {
        return DB::table('posts')
            ->where('id', $post_id)
            ->where(function($query) use ($page_id) {
                return $query->where('author_id', $page_id)
                             ->orWhere('page_id', $page_id);
            })
            ->take(1)
            ->delete();

    }

    public static function getOne($post_id)
    {
        return Post::join('users AS author', 'author.page_id', '=', 'posts.author_id')
            ->join('users AS recipient', 'recipient.page_id', '=', 'posts.page_id')
            ->where('posts.id', $post_id)
            ->select(Post::$test)
            ->first();
    }
}
