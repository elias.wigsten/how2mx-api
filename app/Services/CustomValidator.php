<?php

namespace App\Services;

use Illuminate\Validation\Validator as Validator;
use Carbon\Carbon;

class CustomValidator extends Validator {
    /** Extended custom validators
     *
     * @param $attribute
     * @param $value ( The value that is under validation)
     * @param array $parameters ( Certain parameters that is given to the validator )
     * @return bool
     */

    public function validateFileExtensions($attribute, $value, $parameters = array()) {
        foreach ($value AS $file) {
            $file_ext = explode('/', $file->getMimeType());
            $result = false;

            if (in_array($file_ext[1], $parameters)) {
                $result = true;
                continue;
            }

            if (!$result) {
                return false;
            }
        }

        return true;
    }

    // Validate if the provided birth date is younger than the oldest age-limit ( default = 100 years )
    public function validateYoungerThan($attribute, $value, $parameters = array(100))
    {
        $maxAge = (int)$parameters[0];

        return Carbon::createFromFormat('Y-m-d', $value)->age <= $maxAge;
    }

    // Validate if the provided birth date is older than the lower age-limit ( default = 13 years )
    public function validateOlderThan($attribute, $value, $parameters = array(13))
    {
        $minAge = (int)$parameters[0];

        return Carbon::createFromFormat('Y-m-d', $value)->age >= $minAge;
    }

    // Validate if the provided birth date is within the age-limit ( default min = 13 | default max = 100 )
    public function validateAgeBetween($attribute, $value, $parameters = array(13, 100))
    {
        $minAge = (int)$parameters[0];
        $maxAge = (int)$parameters[1];

        return Carbon::createFromFormat('Y-m-d', $value)->age <= $maxAge && Carbon::createFromFormat('Y-m-d', $value)->age >= $minAge;
    }

    // Validate if the provided name uses valid characters (A-Ö, a-ö and -)
    public function validateName($attribute, $value, $parameters)
    {
        return preg_match('/^[A-Za-zÅ-Öå-ö-]+$/', $value, $matches, PREG_OFFSET_CAPTURE) === 1;
    }

    /**
     * Define variables that are used in the translation file for the validation error message
     *
     * Validation error message path: resources/lang/{en/se}/validation.php
     *
     * @param $message = Validation error message
     * @param $attribute = Name of the validated field (the index position name)
     * @param $rule = Name of the validation function
     * @param $parameters = Parameters given in the validation request
     * @return mixed
     */

    public function replaceFileExtensions($message, $attribute, $rule, $parameters)
    {
        $extensions = array();
        foreach ($parameters AS $extension) {
            $extensions[] = '.' . $extension;
        }

        return str_replace(':extensions', implode(', ', $extensions), $message);
    }

    // Give the younger_than error message a :max variable with the oldest allowed age
    public function replaceYoungerThan($message, $attribute, $rule, $parameters)
    {
        return str_replace(':max', $parameters[0], $message);
    }

    //Give the older_than error message a :min variable with the youngest allowed age
    public function replaceOlderThan($message, $attribute, $rule, $parameters)
    {
        return str_replace(':min', $parameters[0], $message);
    }

    // Give the age_between error message a :min and :max variable with the youngest and oldest allowed date
    public function replaceAgeBetween($message, $attribute, $rule, $parameters)
    {
        return str_replace(array(':min', ':max'), array($parameters[0], $parameters[1]), $message);
    }
}