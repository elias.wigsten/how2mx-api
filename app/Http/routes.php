<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('register', 'Auth\AuthController@postRegister');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('remembered', 'Auth\AuthController@getRemembered');
Route::post('logout', 'Auth\AuthController@postLogout');

Route::get('user', 'UserController@getUserPage');
Route::post('sendFriendRequest', 'UserController@sendFriendRequest')->middleware(['auth']);
Route::post('removeFriend', 'UserController@removeFriend')->middleware(['auth']);
Route::post('acceptFriend', 'UserController@acceptFriend')->middleware(['auth']);
Route::get('user/friends', 'UserController@getFriends');
Route::post('user/post', 'PostController@create')->middleware(['auth']);
Route::get('user/posts', 'PostController@getUserPosts');
Route::post('user/post/delete', 'PostController@delete')->middleware(['auth']);

Route::post('user/gallery/create', 'Media\GalleryController@createForUser')->middleware(['auth']);
Route::post('user/gallery/item', 'Media\GalleryController@addItem');
Route::post('user/gallery/item/description', 'Media\GalleryController@updateItemDescription');
Route::post('user/gallery/items/delete', 'Media\GalleryController@deleteItems');

Route::get('user/gallery/items', 'Media\GalleryController@getItems');
Route::get('user/gallery/items/next', 'Media\GalleryController@getNextItems');

Route::post('user/gallery/title', 'Media\GalleryController@setTitle');
Route::get('user/gallery', 'Media\GalleryController@getGallery');
Route::get('user/galleries', 'Media\GalleryController@getGalleries');

Validator::extend('younger_than', 'CustomValidator@validateYoungerThan');
Validator::extend('older_than', 'CustomValidator@validateOlderThan');
Validator::extend('age_between', 'CustomValidator@validateAgeBetween');
