<?php

namespace App\Http\Middleware;

use Closure;

class FileUpload
{
    private $unsupportedFiles = [];
    private $images = [];
    private $videos = [];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($request->files)) {
            $descriptions = json_decode($request->descriptions);

            for ($i = 0; $i < count($request->files); $i++) {
                $mimeType = explode('/', $request->file('file' . $i)->getClientMimeType());

                $file = [
                    'file'          => $request->file('file' . $i),
                    'description'   => $descriptions[$i]
                ];

                switch ($mimeType[0]) {
                    case 'image':
                        $this->images[] = $file;
                        break;
                    case 'video':
                        $this->videos[] = $file;
                        break;
                        break;
                }
            }

            if (count($this->images) + count($this->videos) > 0) {
                $this->update($request);
                return $next($request);
            } else {
                $response['unsupported_files'] = $this->unsupportedFiles;
            }

            return $response;
        }

        return null;
    }

    private function update($request) {
        $request->merge([
            'unsupported_files' => $this->unsupportedFiles,
            'images'            => $this->images,
            'videos'            => $this->videos
        ]);
    }
}
