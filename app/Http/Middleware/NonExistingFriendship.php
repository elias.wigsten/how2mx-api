<?php

namespace App\Http\Middleware;

use App\Models\Friendship;
use Closure;
use Illuminate\Support\Facades\Auth;

class NonExistingFriendship
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $self_id = Auth::user()->id;
        $user_id = (int)$request->user_id;

        if ($self_id !== $user_id) {
            $friendship = Friendship::existsBetween(Auth::user()->id, $request->user_id);

            if (!$friendship) {
                return $next($request);
            }
        }

        return response()->json()->setStatusCode(401);
    }
}
