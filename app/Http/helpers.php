<?php

use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

function flash($message = null, $title = null)
{
    $flash = app('App\Http\Flash');

    if (func_num_args() == 0) {
        return $flash;
    }

    return $flash->default($message, $title);
}

/**
 * Calculate a users age based on date
 *
 * @param $birth_date
 * @return int|void (Age)
 */
function user_age($birth_date)
{
    $date = date_parse($birth_date);

    return Carbon::createFromDate($date['year'], $date['month'], $date['day'])->age;
}

/**
 * Convert a name to uppercase
 * Including double names separated with "space" and "hyphen"
 *
 * @param $name
 * @return string
 */
function uc_name($name)
{
    $name_parts = preg_split('/\-/', $name);

    if (is_array($name_parts) && count($name_parts) > 1) {
        $name = build_uc_name($name_parts, '-');
    }
    else {
        $name_parts = preg_split('/\s/', $name);

        if (is_array($name_parts) && count($name_parts) > 1) {
            $name = build_uc_name($name_parts, ' ');
        }
        else {
            $name = ucfirst($name);
        }
    }

    return $name;
}

/**
 * Build an uppercase name
 *
 * @param $name_parts
 * @param $separator
 *
 * @return string (finished uppercase name)
 */
function build_uc_name($name_parts, $separator) {

    $name = ucfirst($name_parts[0]);

    foreach ($name_parts as $key => $val) {
        if ($key !== 0) {
            $name .= $separator . ucfirst($val);
        }
    }

    return $name;
}

function get_friendship_btn_name($friendship) {

    switch ($friendship) {
        case 1:
            return 'Add friend';
        case 2:
            return 'Pending';
        case 3:
            return 'Remove friend';
    }

    return false;
}

function get_friendship_btn_id($friendship) {

    switch ($friendship) {
        case 1:
            return 'add_friend_btn';
        case 2:
            return 'pending_friend_request_btn';
        case 3:
            return 'remove_friend_btn';
    }

    return false;
}