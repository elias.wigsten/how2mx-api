<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getUserPosts(Request $request)
    {
        $posts = Post::getUserPosts($request->page_id);

        $response = [
            'isRecipient' => Auth::check() && Auth::user()->page_id == $request->page_id,
            'posts' => $posts
        ];

        return json_encode($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $current_time = Carbon::now();

        $post = new Post();
        $post->author_id = Auth::user()->page_id;
        $post->page_id = $request->page_id;
        $post->message = $request->message;
        $post->created_at = $current_time;
        $post->updated_at = $current_time;
        $post->save();

        $response = [
            'isRecipient' => Auth::user()->page_id == $request->page_id,
            'post'  => Post::getOne($post->id)
        ];

        return json_encode($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $result = Post::remove(Auth::user()->page_id, $request->post_id);

        if ($result != 1) {
            abort(403);
        }

        return response();
    }
}
