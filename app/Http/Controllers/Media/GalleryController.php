<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Controller;
use App\Models\MediaItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Utils\FileHandler;
use App\Models\Gallery;

class GalleryController extends Controller
{
    private $take = 20;

    public function setTitle(Request $request)
    {
        $result = Gallery::where('page_id', Auth::user()->page_id)
            ->where('id', $request->gallery_id)
            ->update(['title' => $request->title]);

        if ($result != 1) {
            abort(403);
        }
    }

    public function createForUser()
    {
        $gallery = Gallery::createNew();

        return json_encode($gallery);
    }

    public function addItem(Request $request)
    {
        $fileHandler = new FileHandler(
            ['pages', Auth::user()->page_id, $request->gallery_id]);

        $fileHandler->resizeAndSaveImageToDisk($request->file);
        $item = MediaItem::newImg(
            $request->gallery_id,
            $fileHandler->getFileSrc());

        return json_encode($item);
    }

    public function getItems(Request $request)
    {
        if (Auth::check()) {
            if ($request->page_id === Auth::user()->page_id) {
                $this->take = 19;
            }
        }

        $response = [];

        if (!empty($request->to)) {
            $items = MediaItem::where('owner_id', $request->page_id)
                ->where('gallery_id', $request->gallery_id)
                ->where('id', '>', $request->to)
                ->orderBy('id', 'desc')
                ->get();
        } else {
            $items = MediaItem::where('owner_id', $request->page_id)
                ->where('gallery_id', $request->gallery_id)
                ->orderBy('id', 'desc')
                ->take($this->take)
                ->get();

            if (count($items) > 0) {
                $response['canRefresh'] = MediaItem::itemsAfter(
                    $request->page_id, $request->gallery_id, $items->last()->id);
            } else {
                $response['canRefresh'] = false;
            }
        }

        $response['items'] = $items;

        return json_encode($response);
    }

    public function getNextItems(Request $request)
    {
        $response = [];

        $items = MediaItem::where('owner_id', $request->page_id)
            ->where('gallery_id', $request->gallery_id)
            ->where('id', '<', $request->from)
            ->orderBy('id', 'desc')
            ->take($this->take)
            ->get();

        if (count($items) > 0) {
            $response['canRefresh'] = MediaItem::itemsAfter(
                $request->page_id, $request->gallery_id, $items->last()->id);
        } else {
            $response['canRefresh'] = false;
        }

        $response['items'] = $items;

        return json_encode($response);
    }

    public function getGallery(Request $request)
    {
        $fields = [
            'galleries.id',
            'galleries.page_id',
            'galleries.title',
            'media_items.src AS preview',
            'galleries.created_at',
            'galleries.updated_at'
        ];

        $gallery = Gallery::where('galleries.page_id', $request->page_id)
            ->where('galleries.id', $request->gallery_id)
            ->join('media_items', function($join) {
                $join->on('galleries.preview', '=', 'media_items.id')
                    ->orOn('galleries.id', '=', 'media_items.gallery_id');
            }, 'outer')->groupBy('galleries.id')
            ->with(['items' => function($query) {
                $query->orderBy('id', 'desc')
                    ->take($this->take);
            }])
            ->first($fields);

        $response['gallery'] = $gallery;

        $last = $gallery->items->last();

        if ($last) {
            $response['canRefresh'] = MediaItem::itemsAfter(
                $gallery->page_id, $gallery->id, $last->id);
        }

        return json_encode($response);
    }

    public function deleteItems(Request $request)
    {
        MediaItem::where('owner_id', Auth::user()->page_id)
                ->where('gallery_id', $request->gallery_id)
                ->whereIn('id', $request->items)
                ->delete();
    }

    public function updateItemDescription(Request $request)
    {
        $result = MediaItem::where('owner_id', Auth::user()->page_id)
            ->where('id', $request->id)
            ->update(['description' => $request->description]);

        if ($result != 1) {
            abort(403);
        }
    }

    public function getGalleries(Request $request)
    {
        /*$galleries = Gallery::where('page_id', $request->page_id)
            ->with(['mediaItems' => function($query) {
                $query->orderBy('created_at', 'desc')
                    ->take(1);
            }, 'preview'])
            ->get();*/

        $fields = [
            'galleries.id',
            'galleries.page_id',
            'galleries.title',
            'media_items.src AS preview',
            'galleries.created_at',
            'galleries.updated_at'
        ];

        $galleries = Gallery::where('page_id', $request->page_id)
            ->join('media_items', function($join) {
                $join->on('galleries.preview', '=', 'media_items.id')
                    ->orOn('galleries.id', '=', 'media_items.gallery_id');
            }, 'outer')->groupBy('galleries.id')
            ->take($this->take)
            ->get($fields);

        return json_encode([
            'canRefresh'    => Gallery::canRefresh($galleries->last()),
            'galleries'     => $galleries
        ]);
    }
}