<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Friendship;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getUserPage(Request $request)
    {
        if (Auth::check()) {
            $self = Auth::user();

            if ($self->page_id != $request->page_id) {
                $user = User::getUserForAuthorized($request->page_id, $self->id);
            }
            else {
                $user = $self;
            }
        }
        else {
            $user = User::getUserForGuest($request->page_id);
        }

        return json_encode($user);
    }

    public function sendFriendRequest(Request $request)
    {
        $friendship = Friendship::addFriend(Auth::user()->id, $request->user_id);

        if (empty($friendship)) {
            abort(401);
        }

        return json_encode($friendship);
    }

    public function removeFriend(Request $request)
    {
        $wasRemoved = Friendship::removeFriend(Auth::user()->id, $request->user_id);

        if (!$wasRemoved) {
            abort(401);
        }
    }

    public function acceptFriend(Request $request)
    {
        $friendship = Friendship::acceptFriend(Auth::user()->id, $request->user_id);

        if (!$friendship) {
            abort(401);
        }

        return json_encode($friendship);
    }

    public function getFriends(Request $request)
    {
        if (Auth::check() && (Auth::user()->id == $request->user_id)) {
            $friends = Friendship::getSourceUserFriends(Auth::user()->id);
        } else {
            $friends = Friendship::getUserFriends($request->user_id);
        }

        $response = [
            'sourceOwner'   => Auth::check() && Auth::user()->id == $request->user_id,
            'friends'       => $friends
        ];

        return json_encode($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
