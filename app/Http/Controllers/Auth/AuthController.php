<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest as CreateUserRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Page;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['postLogout', 'getRemembered']]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function postRegister(CreateUserRequest $request)
    {
        $request->page_id = Page::createPage();

        User::createUser($request);
    }

    public function postLogin(Request $request)
    {
        $user = Auth::attempt($request->only(['email', 'password']), $request->remember);

        if (!$user) {
            abort(401);
        }

        return json_encode(Auth::user());
    }

    public function getRemembered()
    {
        $user = null;

        if (Auth::check() || Auth::viaRemember()) {
            $user = json_encode(Auth::user());
        }

        if ($user === null) {
            abort(203);
        }

        return $user;
    }

    public function postLogout()
    {
        Auth::logout();
    }
}