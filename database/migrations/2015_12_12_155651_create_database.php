<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Add tables
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 36);
            $table->string('page_id', 36);
            $table->unsignedInteger('profile_img')->nullable(true);
            $table->string('f_name', 25);
            $table->string('l_name', 25);
            $table->unsignedSmallInteger('gender');
            $table->string('email', 255);
            $table->date('birth_date');
            $table->unsignedInteger('phone')->nullable(true);
            $table->unsignedInteger('address_id')->nullable(true);
            $table->unsignedInteger('track_id')->nullable(true);
            $table->string('password', 60);
            $table->rememberToken();
            $table->string('activation_code', 60)->nullable();
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        Schema::create('pages', function (Blueprint $table) {
            $table->string('id', 36);
            $table->string('page_name', 36)->nullable(true);
            $table->unsignedInteger('page_type_id')->default(1);
            $table->timestamps();
        });

        Schema::create('page_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_type', 15);
        });

        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country', 25);
            $table->string('region', 25);
            $table->string('city', 25);
            $table->unsignedSmallInteger('zip_code');
            $table->string('street_name', 40);
            $table->unsignedSmallInteger('street_number');
            $table->timestamps();
        });

        Schema::create('galleries', function (Blueprint $table) {
            $table->string('id', 36);
            $table->string('page_id', 36);
            $table->string('preview')->nullable(true);
            $table->string('title', 35);
            $table->timestamps();
        });

        Schema::create('media_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gallery_id', 36)->nullable(true);
            $table->string('owner_id', 36)->nullable(true);
            $table->unsignedInteger('media_type');
            $table->string('src', 40);
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('media_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('media_type');
        });

        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author_id', 36);
            $table->string('page_id', 36);
            $table->string('message');
            $table->timestamps();
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('conversation_id');
            $table->string('title', 40);
            $table->string('message');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });

        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page1', 36);
            $table->string('page2', 36);
            $table->timestamps();
        });

        Schema::create('friendships', function (Blueprint $table) {
            $table->string('user1', 36);
            $table->string('user2', 36);
            $table->unsignedInteger('status')->default(0);
        });

        Schema::create('friendship_statuses', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->string('friendship_status', 20);
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email');
            $table->string('token');
            $table->timestamp('created_at');
        });

        // ********************************************************************************************* //

        // Add table settings

        // ********************************************************************************************* //

        Schema::table('users', function($table) {
            // Unique columns
            $table->primary('id');
            $table->unique('email');
            $table->unique('phone');
            $table->unique('activation_code');

            // Indexed columns
            $table->index('f_name');
            $table->index('l_name');
            $table->index('birth_date');
        });

        Schema::table('pages', function ($table) {
            $table->primary('id');
            $table->unique('page_name');
        });

        Schema::table('page_types', function ($table) {
            $table->unique('page_type');
        });

        Schema::table('addresses', function ($table) {
            $table->index('country');
            $table->index('region');
            $table->index('city');
            $table->index('zip_code');
        });

        Schema::table('galleries', function ($table) {
            $table->primary('id');
            $table->index('page_id');
        });

        Schema::table('conversations', function ($table) {
            $table->unique(array('page1', 'page2'));
        });

        Schema::table('friendships', function ($table) {
            $table->unique(array('user1', 'user2'));
            $table->index('status');
        });

        Schema::table('friendship_statuses', function ($table) {
            $table->unique('id');
            $table->unique('friendship_status');
        });

        Schema::table('password_resets', function ($table) {
            $table->index('email');
            $table->index('token');
        });

        // ********************************************************************************************* //

        // Add foreign keys

        // ********************************************************************************************* //

        Schema::table('users', function ($table) {
            $table->foreign('page_id')->references('id')->on('pages');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('profile_img')->references('id')->on('media_items');
        });

        Schema::table('pages', function ($table) {
            $table->foreign('page_type_id')->references('id')->on('page_types');
        });

        Schema::table('galleries', function ($table) {
            $table->foreign('page_id')->references('id')->on('pages');
        });

        Schema::table('media_items', function ($table) {
            $table->foreign('gallery_id')->references('id')->on('galleries');
            $table->foreign('media_type')->references('id')->on('media_types');
        });

        Schema::table('posts', function ($table) {
            $table->foreign('author_id')->references('id')->on('pages');
            $table->foreign('page_id')->references('id')->on('pages');
        });

        Schema::table('messages', function ($table) {
            $table->foreign('conversation_id')->references('id')->on('conversations');
        });

        Schema::table('conversations', function ($table) {
            $table->foreign('page1')->references('id')->on('pages');
            $table->foreign('page2')->references('id')->on('pages');
        });

        Schema::table('friendships', function ($table) {
            $table->foreign('user1')->references('id')->on('users');
            $table->foreign('user2')->references('id')->on('users');
            $table->foreign('status')->references('id')->on('friendship_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropForeign('users_page_id_foreign');
            $table->dropForeign('users_address_id_foreign');
            $table->dropForeign('users_profile_img_foreign');
        });

        Schema::table('pages', function ($table) {
            $table->dropForeign('pages_page_type_id_foreign');
        });

        Schema::table('galleries', function ($table) {
            $table->dropForeign('galleries_page_id_foreign');
        });

        Schema::table('media_items', function ($table) {
            $table->dropForeign('media_items_gallery_id_foreign');
            $table->dropForeign('media_items_media_type_foreign');
        });

        Schema::table('posts', function ($table) {
            $table->dropForeign('posts_author_id_foreign');
            $table->dropForeign('posts_page_id_foreign');
        });

        Schema::table('messages', function ($table) {
            $table->dropForeign('messages_conversation_id_foreign');
        });

        Schema::table('conversations', function ($table) {
            $table->dropForeign('conversations_page1_foreign');
            $table->dropForeign('conversations_page2_foreign');
        });

        Schema::table('friendships', function ($table) {
            $table->dropForeign('friendships_user1_foreign');
            $table->dropForeign('friendships_user2_foreign');
            $table->dropForeign('friendships_status_foreign');
        });

        Schema::drop('users');
        Schema::drop('pages');
        Schema::drop('page_types');
        Schema::drop('addresses');
        Schema::drop('galleries');
        Schema::drop('media_items');
        Schema::drop('media_types');
        Schema::drop('posts');
        Schema::drop('messages');
        Schema::drop('conversations');
        Schema::drop('friendships');
        Schema::drop('friendship_statuses');
        Schema::drop('password_resets');
    }
}

