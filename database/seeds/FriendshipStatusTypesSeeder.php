<?php

use Illuminate\Database\Seeder;

class FriendshipStatusTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('friendship_statuses')->insert([
            ['id' => 0, 'friendship_status' => 'pending'],
            ['id' => 1, 'friendship_status' => 'accepted']
        ]);
    }
}
