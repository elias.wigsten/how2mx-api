<?php

use Illuminate\Database\Seeder;

class PageTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_types')->insert([
            ['page_type' => 'user'],
            ['page_type' => 'shop'],
            ['page_type' => 'event'],
            ['page_type' => 'track'],
            ['page_type' => 'sponsor']
        ]);
    }
}
