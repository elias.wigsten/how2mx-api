<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use App\Models\User;

class UsersSeeder extends Seeder
{
    private $users = [
        'elias'     => [
            'f_name'        => 'elias',
            'l_name'        => 'wigsten',
            'gender'        => 0,
            'email'         => 'elias.wigsten@how2mx.se',
            'birth_date'    => '1989-04-28',
        ],
        'mattias'   => [
            'f_name'        => 'mattias',
            'l_name'        => 'falk',
            'gender'        => 0,
            'email'         => 'mattias.falk@how2mx.se',
            'birth_date'    => '1978-04-12',
        ],
        'adrian' => [
            'f_name'        => 'adrian',
            'l_name'        => 'wigsten',
            'gender'        => 0,
            'email'         => 'adrian@gmail.com',
            'birth_date'    => '1987-02-28',
        ],
        'sandra' => [
            'f_name'        => 'sandra',
            'l_name'        => 'hjerpe',
            'gender'        => 1,
            'email'         => 'sandra@gmail.com',
            'birth_date'    => '1986-12-15',
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users AS $u) {
            $page = Page::create();

            $user = new User();
            $user->id = \App\Utils\Generator::UUID();
            $user->f_name = $u['f_name'];
            $user->l_name = $u['l_name'];
            $user->page_id = $page->id;
            $user->gender = $u['gender'];
            $user->email = $u['email'];
            $user->birth_date = $u['birth_date'];
            $user->password = bcrypt('hejsan');
            $user->save();
        }
    }
}
