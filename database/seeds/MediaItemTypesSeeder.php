<?php

use Illuminate\Database\Seeder;

class MediaItemTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_types')->insert([
            ['media_type' => 'img'],
            ['media_type' => 'vid']
        ]);
    }
}
